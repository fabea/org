;;; publish.el --- publish script for orgmode        -*- lexical-binding: t; -*-

;;; Commentary:
;; Copyright (C) 2020  Alex Yan

;; Author: Alex Yan <yhashtur@yandex.com>
;; Keywords: web, org, generator

;;; Code:
(require 'package)
(package-initialize)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'org-plus-contrib)
(package-install 'htmlize)
(package-install 'ox-twbs)
(package-install 'hydandata-light-theme)
(package-install 'material-theme)
(package-install 'yaml-mode)

(load-theme 'hydandata-light t)
(load-theme 'material-light t)
(require 'org)
(require 'ox-publish)
(require 'ox-twbs)
(require 'yaml-mode)

;; setting to nil, avoids "Author: x" at the bottom
(setq user-full-name "Alex Yan")

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-doctype "html5")

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (lisp       . t)))

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

(setq org-publish-project-alist
      (list
       (list "org-site"
             :base-directory "."
             :base-extension "org"
             :recursive t
             :with-date t
             :with-title t
             :publishing-function '(org-twbs-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft"))
             :html-preamble "
<nav class=\"navbar navbar-default\" role=\"navigation\">
    <div class=\"container-fluid\">
    <div class=\"navbar-header\">
        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\"
                data-target=\"#example-navbar-collapse\">
            <span class=\"sr-only\">Menu</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
        </button>
        <a class=\"navbar-brand\" href=\"/index.html\">Yan's Life Organized</a>
    </div>
    <div class=\"collapse navbar-collapse\" id=\"example-navbar-collapse\">
        <ul class=\"nav navbar-nav\">
			<li><a href=\"/school.html\">School Chores</a></li>
                        <li><a href=\"/personal.html\">Personal Lives</a></li>
			<li><a href=\"/inbox.html\">Uncategorized Items</a></li>
			<li><a href=\"/emacs.html\">Emacs Main Config</a></li>
            </ul>
    </div>
    </div>
</nav>"
             :auto-sitemap t
             :sitemap-title "Yan's Life Organized"
             :sitemap-filename "index.org"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/images/favicon.ico\"/>"
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically)
       (list "site-static"
             :base-directory "images"
             :base-extension site-attachments
             :publishing-directory "./public/images"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("org-site" "site-static"))))

(provide 'publish)
;;; publish.el ends here
